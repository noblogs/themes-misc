<?php global $zenlite_options;?>

<div class="navbar">

<?php if (!dynamic_sidebar( 'Main menu' ) ) : ?>

<?php
$args = array(
	'theme_location' => 'primary',
	'container' => ''
);
if( isset( $zenlite_options['menu_type'] ) && $zenlite_options['menu_type'] == 'cats' ) $args['fallback_cb'] = 'zenlite_callback_cats';
wp_nav_menu( $args );
?>

<?php endif; ?>

</div>
