<?php
if ( have_comments() && post_password_required() ) : ?>
<p class="comments-nopassword"><?php _e( "This entry's comments are password protected. Enter the password into the form above to view the comments.", 'zenlite' ); ?></p>
<?php
// Stop the rest of comments.php from being processed,
return;
endif;
?>

<?php if( have_comments() ) : ?>
<h2 id="comments"><?php
$args = array(
	'zero' => __('No comments', 'zenlite'),
	'one' => __('1 Comment', 'zenlite'),
	'more' => __('% Comments', 'zenlite')
);
comments_number($args ); ?><?php _e(' on ', 'zenlite');?><?php the_title(); ?></h2>
<?php endif;?>

<?php if ('open' == $post->comment_status) : ?>
<ul class="comment-links">

<?php if ('open' == $post->comment_status) : ?><li class="add-comment"><a href="#respond"><?php _e('Add your comment', 'zenlite');?></a></li><?php endif;?>

<?php if (have_comments()) : ?><li class="comments-feed-link"><a href="<?php echo get_post_comments_feed_link();?>"><?php _e('Comments feed for this post', 'zenlite'); ?></a></li><?php endif;?>

<?php if(pings_open()) : ?><li class="trackback-link"><a href="<?php trackback_url();?>" rel="trackback"><?php _e('TrackBack', 'zenlite');?> <abbr title="<?php _e('Uniform Resource Identifier', 'zenlite');?>"><?php _e('URI', 'zenlite');?></abbr></a></li><?php endif;?>

</ul><?php endif; ?>

<?php if(have_comments()) : ?>

<?php paginate_comments_links('type=list'); ?>

<ol id="commentlist">
<?php wp_list_comments(); ?>
</ol>

<?php paginate_comments_links('type=list'); ?>

<?php endif;

comment_form( zenlite_comment_form_args($user_identity, $post->ID, $req) );