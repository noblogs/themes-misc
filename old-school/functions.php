<?php

/**
* This is your child theme's functions.php file.
* You should make edits and add additional code above this point.
* Only change the functions below if you know what you're doing.
*/

/********************************************************/

/* Load child theme textdomain. */
load_child_theme_textdomain( 'old-school', get_stylesheet_directory() );

/* Actions. */
add_action( 'init', 'old_school_admin_initialize' );
add_action( 'init', 'old_school_remove_actions' );
add_action( 'widgets_init', 'old_school_register_sidebars' );
add_action( 'template_redirect', 'old_school_enqueue_script' );
add_action( 'template_redirect', 'old_school_enqueue_style' );
add_action( 'hybrid_header', 'get_search_form' );

/* Filters. */
add_filter( 'hybrid_post_meta_box', 'old_school_post_meta_box' );

/**
 * Removes Hybrid-specific default actions.
 *
 * @since 0.2
 */
function old_school_remove_actions() {
	remove_action( 'hybrid_before_content', 'hybrid_breadcrumb' );
}

/**
 * Registers additional widget areas for the child theme.
 *
 * @since 0.3
 */
function old_school_register_sidebars() {
	register_sidebar( array( 'name' => __( 'Utility: Feature', 'old-school'), 'id' => 'utility-feature', 'description' => __( 'A widget area used on the Showcase page template in the feature area.', 'old-school' ), 'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-inside">', 'after_widget' => '</div></div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
}

/**
 * Adds additional meta box settings for the post settings on the post editor.
 *
 * @since 0.3
 */
function old_school_post_meta_box( $meta ) {
	$meta['full'] = array( 'name' => 'Full', 'title' => __( 'Feature/Full:', 'old-school'), 'type' => 'text' );
	return $meta;
}

/**
 * Loads JavaScript required by the theme.
 *
 * @since 0.2
 */
function old_school_enqueue_script() {
	if ( is_page_template( 'page-showcase.php' ) || is_page_template( 'showcase.php' ) )
		wp_enqueue_script( 'old-school', get_stylesheet_directory_uri() . '/js/old-school.js', array( 'jquery' ), 0.1 );
}

/**
 * Loads CSS required by the theme.
 *
 * @since 0.2
 */
function old_school_enqueue_style() {
	if ( is_page_template( 'page-showcase.php' ) || is_page_template( 'showcase.php' ) )
		wp_enqueue_style( 'page-showcase', get_stylesheet_directory_uri() . '/page-showcase.css', false, 0.1, 'screen' );
}

/* == The functions below deal with internal functionality to Old School and should not be tampered with. == */

/**
 * Intializes extra admin actions.
 *
 * @since 0.2
 */
function old_school_admin_initialize() {
	if ( is_admin() ) {
		add_action( 'admin_menu', 'old_school_settings_meta_box' );
		add_action( 'hybrid_update_settings_page', 'old_school_save_settings_meta_box' );
	}
}

/**
 * Adds child theme-specific meta boxes to the theme settings page.
 *
 * @since 0.2
 */
function old_school_settings_meta_box() {
	add_meta_box( 'old-school-showcase', __( 'Showcase page template settings', 'old-school' ), 'old_school_showcase_meta_box', 'appearance_page_theme-settings', 'normal', 'low' );
}

/**
 * Saves the Old School theme settings on the theme settings page.
 *
 * @since 0.2
 */
function old_school_save_settings_meta_box() {

	/* Verify the nonce, so we know this is secure. */
	if ( !wp_verify_nonce( $_POST['old_school_meta_box_nonce'], basename( __FILE__ ) ) )
		return false;

	$options = get_option( 'old_school_theme_settings' );

	$options['feature_cat'] = strip_tags( $_POST['feature_cat'] );
	$options['tab_1_cat'] = strip_tags( $_POST['tab_1_cat'] );
	$options['tab_2_cat'] = strip_tags( $_POST['tab_2_cat'] );
	$options['tab_3_cat'] = strip_tags( $_POST['tab_3_cat'] );
	$options['tab_4_cat'] = strip_tags( $_POST['tab_4_cat'] );
	$options['tab_5_cat'] = strip_tags( $_POST['tab_5_cat'] );
	$options['tab_6_cat'] = strip_tags( $_POST['tab_6_cat'] );

	$updated = update_option( 'old_school_theme_settings', $options );
}

/**
 * Displays the showcase meta box for the theme settings page.
 *
 * @since 0.2
 */
function old_school_showcase_meta_box() {
	$settings = get_option( 'old_school_theme_settings' );
	$cats = get_categories( array( 'type' => 'post' ) ); ?>

	<!-- Security! Very important! -->
	<input type="hidden" name="old_school_meta_box_nonce" value="<?php echo wp_create_nonce( basename( __FILE__ ) ); ?>" />

	<table class="form-table">
		<tr>
			<th><label for="feature_cat"><?php _e( 'Feature:', 'old-school' ); ?></label></th>
			<td>
				<?php _e( 'Category:', 'old-school' ); ?> 
				<select id="feature_cat" name="feature_cat">
					<option value=""></option>
					<?php foreach ( $cats as $cat ) { ?>
						<option <?php if ( $cat->term_id == $settings['feature_cat'] ) echo ' selected="selected"'; ?> value="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<tr>
			<th><label for="tab_1_cat"><?php _e( 'Tabs:', 'old-school' ); ?></label></th>
			<td>

			<?php $tabs = array( 1, 2, 3, 4, 5, 6 );

			foreach ( $tabs as $tab ) { ?>
				<?php printf( __( 'Tab %1$s Category:', 'old-school' ), $tab ); ?>
				<select id="tab_<?php echo $tab; ?>_cat" name="tab_<?php echo $tab; ?>_cat">
					<option value=""></option>
					<?php foreach ( $cats as $cat ) { ?>
						<option <?php if ( $cat->term_id == $settings["tab_{$tab}_cat"] ) echo ' selected="selected"'; ?> value="<?php echo $cat->term_id; ?>"><?php echo $cat->name; ?></option>
					<?php } ?>
				</select>
				<br />
			<?php }

			?></td>
		</tr>
	</table><?php
}

/**
 * Displays an unorderd list of "tabs" for the Showcase page template's tabbed category section.
 *
 * @since 0.2
 */
function old_school_tabs_loop( $tabs ) {
	$j = 1;
	foreach ( $tabs as $tab ) {
		if ( !empty( $tab ) ) {
			$term = get_term( $tab, 'category' );
			echo '<li class="t' . $j . '"><a class="t' . $j . '" title="' . esc_attr( $term->name ) . '">' . $term->name . '</a></li>';
			++$j;
		}
	}
}

?>