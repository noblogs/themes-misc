msgid ""
msgstr ""
"Project-Id-Version: Old School Child Theme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2010-07-06 19:22-0600\n"
"PO-Revision-Date: 2010-07-06 19:22-0600\n"
"Last-Translator: Justin Tadlock <justin@justintadlock.com>\n"
"Language-Team: ThemeHybrid\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: _e;__;esc_attr_e;esc_attr__;esc_html_e;esc_html__;_x;_ex;esc_attr_x;esc_html_x;_n;_nx;_n_noop;_nx_noop\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"

#: functions.php:40
msgid "Utility: Feature"
msgstr ""

#: functions.php:40
msgid "A widget area used on the Showcase page template in the feature area."
msgstr ""

#: functions.php:49
msgid "Feature/Full:"
msgstr ""

#: functions.php:93
msgid "Showcase page template settings"
msgstr ""

#: functions.php:134
msgid "Feature:"
msgstr ""

#: functions.php:136
msgid "Category:"
msgstr ""

#: functions.php:146
msgid "Tabs:"
msgstr ""

#: functions.php:152
#, php-format
msgid "Tab %1$s Category:"
msgstr ""

#: searchform.php:18
msgid "Search this site..."
msgstr ""

#: searchform.php:19
msgid "Go"
msgstr ""

