<?php
/**
 * Template Name: Showcase
 *
 * This template has a tabbed section of posts by category. Plus, a feature section and widget are 
 * attached to any page using this. You can choose your settings from the Hybrid Settings page.
 *
 * @package OldSchool
 * @subpackage Template
 */

get_header();

/* Get Old School theme settings. */
$settings = get_option( 'old_school_theme_settings' );

/* Set up some default variable for iteration. */
$tab_iterator = 1;
$feature_post_iterator = 1;
$feature_list_iterator = 1;

/* Add the individual tabs to the $tabs array. */
$tabs = array( $settings['tab_1_cat'], $settings['tab_2_cat'], $settings['tab_3_cat'], $settings['tab_4_cat'], $settings['tab_5_cat'], $settings['tab_6_cat'] ); ?>

	<!-- Featured Area. -->
	<?php $loop = new WP_Query( array( 'cat' => $settings['feature_cat'], 'posts_per_page' => 4 ) ); ?>

	<?php if ( $loop->have_posts() ) : ?>

		<div id="feature">

			<div class="feature">

				<!-- Featured images. -->
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

					<div class="<?php hybrid_entry_class( "t{$feature_post_iterator}" ); ?>">
						<?php get_the_image( array( 'custom_key' => array( 'Feature Image', 'Medium', 'Full' ), 'size' => 'medium', 'default_image' => get_stylesheet_directory_uri() . '/images/feature-default.jpg' ) ); ?>
					</div>

					<?php ++$feature_post_iterator; ?>

				<?php endwhile; ?>

				<!-- Featured post list. -->
				<?php $loop = new WP_Query( array( 'cat' => $settings['feature_cat'], 'posts_per_page' => 4 ) ); ?>

				<?php if ( $loop->have_posts() ) : ?>

					<div class="feature-list">
						<ul>

						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

							<?php the_title( '<li class="t' . $feature_list_iterator . '"><a class="t' . $feature_list_iterator . '" title="' . the_title_attribute( 'echo=0' ) . '">', '</a></li>' ); ?>

							<?php ++$feature_list_iterator; ?>

						<?php endwhile; ?>

						</ul>
					</div>

				<?php endif; ?>

			</div>

			<?php dynamic_sidebar( 'utility-feature' ); ?>
		</div>

	<?php endif; ?>
	<!-- End featured area. -->

	<div id="content" class="hfeed content">

		<?php hybrid_before_content(); // Before content hook ?>

		<!-- Tabbed posts area. -->
		<div id="post-tabs" class="post-tabs">

			<div class="tabbed">

				<ul class="tabs">
					<?php old_school_tabs_loop( $tabs ); ?>
				</ul>

				<?php foreach ( $tabs as $category ) : ?>

					<?php if ( $category ) : ?>

						<?php $loop = new WP_Query( array( 'cat' => $category, 'showposts' => 5 ) ); ?>

						<div class="t t<?php echo $tab_iterator; ?> tab-content">

						<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

							<div class="<?php hybrid_entry_class(); ?>">

								<?php get_the_image( array( 'custom_key' => array( 'Thumbnail', 'thumbnail' ), 'size' => 'thumbnail' ) ); ?>

								<?php hybrid_before_entry(); ?>

								<div class="entry-summary">
									<?php the_excerpt(); ?>
								</div>

								<?php hybrid_after_entry(); ?>

							</div>

						<?php endwhile; ?>

						</div>

						<?php ++$tab_iterator; ?>

					<?php endif; ?>

				<?php endforeach; ?>

			</div>

		</div>
		<!-- End tabbed posts area. -->

		<?php wp_reset_query(); // Reset the query ?>

		<?php hybrid_after_singular(); // After page hook ?>

		<?php hybrid_after_content(); // After content hook ?>

	</div><!-- .content .hfeed -->

<?php get_footer(); ?>