	<div id="footer">
		<p class="left">Powered by <strong><a href="http://autistici.org/">R*</a></strong></p>
		<p class="right">A <strong><a href="http://www.techtrot.com/primepress/" title="PrimePress theme homepage">WordPress theme</a></strong> by <strong><a href="http://www.techtrot.com" title="PrimePress author homepage">Ravi Varma</a></strong></p>
	</div><!--#footer-->

</div><!--#container-->	
	
<div class="clear"></div>	
</div><!--#page-->
<?php wp_footer(); ?>
</body>
</html>