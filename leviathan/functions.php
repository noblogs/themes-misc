<?php

/**
 * This is your child theme's functions.php file.
 * You should make edits and add additional code above this point.
 * Only change the functions below if you know what you're doing.
 */

/********************************************************/

/**
 * Uncomment for localization.  Note this theme has no additional text strings.
 */
//load_child_theme_textdomain( 'leviathan', get_stylesheet_directory() . '/languages' );

/**
 * Add theme-specific actions
 */
add_action( 'init', 'leviathan_remove_actions' );
add_action( 'hybrid_after_header', 'hybrid_breadcrumb', 11 );
add_action( 'hybrid_header', 'get_search_form', 11 );
add_action( 'hybrid_singular-post_after_singular', 'leviathan_author_box' );

/**
 * Removes default actions set in place by the parent theme.
 *
 * @since 0.1
 */
function leviathan_remove_actions() {
	remove_action( 'hybrid_before_content', 'hybrid_breadcrumb' );
}

/**
 * Displays an author profile box after singular posts with a link to the author archive, the avatar,
 * and the biographical info from ther user's profile.
 *
 * @since 0.1
 */
function leviathan_author_box() { ?>
	<div class="author-profile vcard">

		<?php echo get_avatar( get_the_author_meta( 'user_email' ), '96' ); ?>

		<h4 class="author-name fn n"><?php the_author_posts_link(); ?></h4>

		<div class="author-description author-bio">
			<?php the_author_meta( 'description' ); ?>
		</div>
	</div><?php
}

?>