<?php 
/**
 * @package WordPress
 * @subpackage Elegant Grunge
 */
?>

<?php
/**
 * Check layout option and display sidebars as needed
 */
	$options = get_option( 'elegant_grunge_theme_options' );
	$current_layout = $options['theme_layout'];

	if ( 'no-sidebar' == $current_layout )
		return;
?>

<div id="sidebar" class="sidebar">
	<ul>
	<?php if ( !  dynamic_sidebar( 'sidebar-1' ) ) : ?>
		<?php if ( is_404() || is_category() || is_day() || is_month() || is_year() || is_search() || is_paged() ) : ?>	 
		<li>
			<?php /* If this is a 404 page */ if ( is_404() ) { ?>
			<?php /* If this is a category archive */ } elseif ( is_category() ) { ?>
			<p>
			<?php printf( __( 'You are currently browsing the archives for the %s category.', 'elegant-grunge' ), single_cat_title( '', false ) ); ?>
			</p>
			<?php /* If this is a yearly archive */ } elseif ( is_day() ) { ?>
			<p>
				<?php printf( __( 'You are currently browsing the %1$s blog archives for the day %2$s', 'elegant-grunge' ), '<a href="'.get_bloginfo( 'url' ) . '/">'.get_bloginfo( 'name' ) . '</a>', get_the_time( __( 'l, F jS, Y', 'elegant-grunge' ) ) ); ?>
			</p>
			<?php /* If this is a monthly archive */ } elseif ( is_month() ) { ?>
			<p>
				<?php printf( __( 'You are currently browsing the %1$s blog archives for %2$s', 'elegant-grunge' ), '<a href="'.get_bloginfo( 'url' ) . '/">'.get_bloginfo( 'name' ) . '</a>', get_the_time( __( 'F, Y', 'elegant-grunge' ) ) ); ?>
			</p>
			<?php /* If this is a yearly archive */ } elseif ( is_year() ) { ?>
			<p>
				<?php printf( __( 'You are currently browsing the %1$s blog archives for the year %2$s', 'elegant-grunge' ), '<a href="'.get_bloginfo( 'url' ) . '/">'.get_bloginfo( 'name' ) . '</a>', get_the_time( __( 'Y', 'elegant-grunge' ) ) ); ?>
			</p>
			<?php /* If this is a monthly archive */ } elseif ( is_search() ) { ?>
			<p>
				<?php printf( __( 'You have searched the %1$s blog archives for <strong>%2$s</strong>. If you are unable to find anything in these search results, you can try one of these links.', 'elegant-grunge' ), '<a href="'.get_bloginfo( 'url' ) . '/">'.get_bloginfo( 'name' ) . '</a>', get_search_query() ); ?>
			</p>
			<?php /* If this is a monthly archive */ } elseif ( isset ( $_GET['paged'] ) && ! empty ( $_GET['paged'] ) ) { ?>
			<p>
				<?php printf( __( 'You are currently browsing the %s blog archives', 'elegant-grunge' ), '<a href="'.get_bloginfo( 'url' ) . '/">'.get_bloginfo( 'name' ) . '</a>' ); ?>
			</p>
			<?php } ?>
		</li>
		<?php endif; ?>
		<li>
			<?php wp_list_pages( 'title_li=<h2>'.__( 'Pages', 'elegant-grunge' ) . '</h2>' ); ?>
		</li>
		<li>
			<h2><?php _e( 'Archives', 'elegant-grunge' )?></h2>
			<ul>
				<?php wp_get_archives( 'type=monthly' ); ?>
			</ul>
		</li>		
		<?php /* If this is the frontpage */ if ( is_home() || is_page() ) : ?>
		<li>
			<?php wp_list_bookmarks(); ?>
		</li>
		<li>
			<h2><?php _e( 'Meta', 'elegant-grunge' )?></h2>
			<ul>
				<?php wp_register(); ?>
				<li>
					<?php wp_loginout(); ?>
				</li>
				<?php wp_meta(); ?>
			</ul>
		</li>
		<?php endif; ?>
	<?php endif; ?>
	</ul>
</div><!-- #sidebar -->

<?php if ( 'content-sidebar-sidebar' == $current_layout ) : ?>
<div id="sidebar-2" class="sidebar">
	<ul>
		<?php if ( ! dynamic_sidebar( 'sidebar-2' ) ):  ?>
		<li>
			<?php get_search_form(); ?>
		</li>
		<li>
			<?php wp_list_categories( 'show_count=1&title_li=<h2>'.__( 'Categories', 'elegant-grunge' ) . '</h2>' ); ?>
		</li>
		<?php endif; ?>
	</ul>
</div><!-- #sidebar-2 -->
<?php endif; ?>