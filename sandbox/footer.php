
	<div id="footer">
		<span id="generator-link"> Proudly powered by <a href="http://autistici.org/" title="<?php _e( 'R*', 'sandbox' ) ?>" rel="generator"><?php _e( 'R*', 'sandbox' ) ?></a></span>
		<span class="meta-sep">|</span>
		<span id="theme-link"><a href="http://www.plaintxt.org/themes/sandbox/" title="<?php _e( 'Sandbox theme for WordPress', 'sandbox' ) ?>" rel="designer"><?php _e( 'Sandbox', 'sandbox' ) ?></a></span>
	</div><!-- #footer -->

</div><!-- #wrapper .hfeed -->

<?php wp_footer() ?>

</body>
</html>
