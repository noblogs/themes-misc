<?php
if ( function_exists('register_sidebar') ){
    // sidebar1
	register_sidebar(array(
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ));
}

$thescenery_defaultsettings = array(
	'backgroundType' => 'keepratio', //stretch, keepratio, original
	
	//backgroundImage => get_bloginfo('template_url') . '/11670011000.jpg',
	'blur' => 'false',
	'blurRate' => '5',
	// options for keepratio backgroundType
	'backgroundHorizontalAlign' => 'left',
	'backgroundVerticalAlign' => 'top',
        
        'pageList' => 'true',
        'searchBox' => 'false'
);
$thescenery_settings = null;
function thescenery_getsettings($force_reload = false){
	global $thescenery_defaultsettings, $thescenery_settings;
	if(!$thescenery_settings || $force_reload){
		if(!get_option('thescenery_settings_initialized')){
			add_option('thescenery_settings_initialized', '1');
			foreach($thescenery_defaultsettings as $k => $v)
				add_option("thescenery_$k", $v);
		}
		$thescenery_settings = array();
		foreach($thescenery_defaultsettings as $k => $default_value){
			$value = get_option("thescenery_$k");
			if(!is_string($value))
				$value = $default_value;
			$thescenery_settings[$k] = $value;
		}
	}
	$thescenery_settings['backgroundImage'] = get_bloginfo('template_url') . '/11670011000.jpg';
	return $thescenery_settings;
}
function thescenery_updatesettings($settings){
	global $thescenery_defaultsettings, $thescenery_settings;
	foreach($thescenery_defaultsettings as $k => $default_value)
		if(is_string($settings[$k]))
			update_option("thescenery_$k", $settings[$k]);
	thescenery_getsettings(true);
}
function thescenery_getsetting($name, $default=''){
  $settings = thescenery_getsettings($name);
  $value = $settings[$name];
  if(!is_string($value))
    return $default;
  return $value;
}
include_once 'theme-options.php';

?>
