<?php

// Hook for adding admin menus
add_action('admin_menu', 'the_scenery_add_admin_page');

// action function for above hook
function the_scenery_add_admin_page() {
	define('FORM_SUBMIT', $_SERVER['REQUEST_METHOD'] == 'POST');	 
	function show_form(){
		$settings = thescenery_getsettings();
                function the_scenery_yesno_radio_buttons($settings, $name, $default = 'false'){
                  $value = $settings[$name];
                  if(!is_string($value))
                    $value = $default;
                ?>
                  <input type="radio" id="<?php echo $name?>_true" name="<?php echo $name?>" value="true" <?php echo $settings[$name] === "true" ? "checked" : "" ?> />
                  <label for="<?php echo $name?>_true"><b>Yes</b></label>
                  <br />
                  <input type="radio" id="<?php echo $name?>_false" name="<?php echo $name?>" value="false" <?php echo $settings[$name] === "true" ? "" : "checked" ?> />
                  <label for="<?php echo $name?>_false"><b>No</b></label>
                  <br />
                <?php
                }
		?>
		<div class="wrap">
			<?php if(FORM_SUBMIT){?><div class="updated fade" id="message"><p><strong>Settings saved.</strong></p></div><?php }?>
			<form method="post">
				<h2>"The Scenery" theme options</h2>
				<h3>Component visibility:</h3>
                                
				<table class="form-table">
                                  <tr valign="top">
                                          <th scope="row">Page list</th> 
                                          <td>
                                            <?php the_scenery_yesno_radio_buttons($settings, "pageList", 'true');?>
                                          </td>
                                  </tr>
                                  <tr valign="top">
                                          <th scope="row">Search box</th> 
                                          <td>
                                            <?php the_scenery_yesno_radio_buttons($settings, "searchBox", 'true');?>
                                          </td>
                                  </tr>
                                </table>
                                        
				<h3>Background image settings:</h3>
				<table class="form-table">
					<tr valign="top">
						<th scope="row">Zoom</th> 
						<td>
							<input type="radio" id="backgroundType_original" name="backgroundType" value="original" <?php echo $settings['backgroundType'] == "original" ? "checked" : "" ?> />
							<label for="backgroundType_original"><b>No zoom</b></label>
							<br />
							<input type="radio" id="backgroundType_keepratio" name="backgroundType" value="keepratio" <?php echo $settings['backgroundType'] == "keepratio" ? "checked" : "" ?> />
							<label for="backgroundType_keepratio"><b>Keep Ratio</b></label>
							<br />
							<input type="radio" id="backgroundType_stretch" name="backgroundType" value="stretch" <?php echo $settings['backgroundType'] == "stretch" ? "checked" : "" ?> />
							<label for="backgroundType_stretch"><b>Stretch</b></label>
							<br />
						</td>
					</tr>
                                        <!--
					<tr valign="top" id="blur_options">
						<th scope="row">Blur</th> 
						<td>
                                                  <?php the_scenery_yesno_radio_buttons($settings, "blur");?>
						</td>
					</tr>
                                        -->
					<tr valign="top" id="alignment_options">
						<th scope="row">Alignment</th> 
						<td>
							<select name="backgroundVerticalAlign">
								<option value="top" <?php echo $settings['backgroundVerticalAlign'] == "top" ? "selected" : "" ?>>Top</option>
								<option value="center" <?php echo $settings['backgroundVerticalAlign'] == "center" ? "selected" : "" ?>>Center</option>
								<option value="bottom" <?php echo $settings['backgroundVerticalAlign'] == "bottom" ? "selected" : "" ?>>Bottom</option>
							</select>
							<select name="backgroundHorizontalAlign">
								<option value="left" <?php echo $settings['backgroundHorizontalAlign'] == "left" ? "selected" : "" ?>>Left</option>
								<option value="center" <?php echo $settings['backgroundHorizontalAlign'] == "center" ? "selected" : "" ?>>Center</option>
								<option value="right" <?php echo $settings['backgroundHorizontalAlign'] == "right" ? "selected" : "" ?>>Right</option>
							</select>
						</td>
					</tr>
				</table>
				<p class="submit">
					<input type="submit" value="Save Changes" name="Submit" />
				</p>
				
			</form>
			<script type="text/javascript">
			function showHideOptions(o){
				var settings = {};
				jQuery(document)
				.find("input[@checked], input[@type='text'], input[@type='hidden'], input[@type='password'], input[@type='submit'], option[@selected], textarea")
				.filter(":enabled")
				.each(function() {
					settings[ this.name || this.id || this.parentNode.name || this.parentNode.id ] = this.value;
				});
				jQuery("#blur_options *").css("visibility", settings.backgroundType == "original" ? "hidden" : "visible");
				jQuery("#alignment_options *").css("visibility", settings.backgroundType == "stretch" ? "hidden" : "visible");
			}
			jQuery(showHideOptions);
			jQuery("input, select").focus(showHideOptions).blur(showHideOptions).click(showHideOptions);;
			</script>
		</div>
		<?php
	}
	function the_scenery_show_options(){
		if(FORM_SUBMIT)
			thescenery_updatesettings($_POST);
		show_form();
	}
	add_theme_page('"The Scenery" theme options', '"The Scenery" theme options', 8, 'the_scenery_show_options', 'the_scenery_show_options');
}
?>