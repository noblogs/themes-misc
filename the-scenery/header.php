<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<style type="text/css" media="screen">
		@import url(<?php bloginfo('stylesheet_url'); ?>);
	</style>
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php wp_get_archives('type=monthly&format=link'); ?>
	<?php //comments_popup_script(); // off by default ?>
	<?php wp_enqueue_script('jquery'); ?>
	<?php wp_head(); ?>
	<style type="text/css">
		<?php
		if(is_page()){
		?>
			/*pages has nothing to do with these things*/
			.widget_categories,
			.widget_archive,
			.widget_tag_cloud,
			.widget_calendar
			{
				display: none;
			}
		<?php
		}?>
	</style>
	<script type="text/javascript">
<!--
<?php
    $blog_url = preg_replace('/\/^/', "", get_bloginfo('url'));
    $http_refer = $_SERVER['HTTP_REFERER'];
    // we don't do opening animation if we are browsing inside wordpress.
    $from_wordpress = substr($http_refer, 0, strlen($blog_url)) == $blog_url ? "true" : "false";
?>
	var fromWordpress=<?php echo $from_wordpress;?>;
// -->
	</script>
</head>
<body>
	<div id="non-background">
		<div id="header">
			<h1 id="header-title">
				<a href="<?php bloginfo('url'); ?>/"><?php bloginfo('name'); ?></a>                    
			</h1>
			<span id="header-description"><?php bloginfo('description'); ?> <a href="<?php bloginfo('rss2_url'); ?>" title="RSS"><img align="absmiddle" alt="RSS" src="<?php bloginfo('template_url'); ?>/images/rss-12px.png" /></a></span>
		</div>
                <script type="text/javascript">
                if(jQuery.browser.mozilla && jQuery.browser.version >= '1.9.1'
                  || jQuery.browser.safari && jQuery.browser.version >= '530.0'){
                  jQuery("#header-title a").css({'text-shadow': '2px 2px 2px #333333', 'color': '#eeeeee'});
                  jQuery("#header-description").css({'text-shadow': '1px 1px 1px #333333'});
                } else{
                  jQuery("#header-title").clone().hide().prependTo("#header").attr("id", "header-title-shadow").css("width", "100%").show();
                  jQuery("#header-title").css({opacity: 0.4});
                }
                </script>
		    <div id="body">
			<div id="content">
<!-- end header -->