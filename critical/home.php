<?php
/**
 * Home Template
 *
 * This template is loaded when on the home/blog page.
 *
 * @package Critical
 * @subpackage Template
 */

get_header(); ?>

	<div class="hfeed content">

		<?php hybrid_before_content(); // Before content hook ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div id="post-<?php the_ID(); ?>" class="<?php hybrid_entry_class(); ?>">

				<?php get_the_image( array( 'custom_key' => array( 'Thumbnail' ), 'size' => 'thumbnail' ) ); ?>

				<?php hybrid_before_entry(); // Before entry hook ?>

				<div class="entry-summary">
					<?php the_excerpt(); ?>
				</div>

				<?php hybrid_after_entry(); // After entry hook ?>

			</div>

			<?php endwhile; ?>

		<?php else: ?>

			<p class="no-data"><?php _e( 'Sorry, no posts matched your criteria.', 'critical' ); ?></p>

		<?php endif; ?>

		<?php hybrid_after_content(); // After content hook ?>

	</div>

<?php get_footer(); ?>