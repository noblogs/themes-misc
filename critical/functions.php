<?php
/**
 * Functions File
 *
 * This is your child theme's functions.php file.  It is here for adding custom functions and 
 * setting up default functionality for this child theme.  You are free to modify this file in
 * any way you like.  Note that if you change the functions already within this file, you run 
 * the risk of losing theme functionality.  Alter the functions below only if you know what
 * you're doing.
 *
 * @package Critical
 * @subpackage Functions
 */

/* Set up the child theme and its default functionality. */
add_action( 'after_setup_theme', 'critical_theme_setup' );

/**
 * Adds all the default actions and filters to their appropriate hooks and sets up anything
 * else needed by the theme.
 *
 * @since 0.2
 */
function critical_theme_setup() {

	/* Get the parent theme prefix for use with its hooks. */
	$prefix = hybrid_get_prefix();

	/* Load translation files. */
	load_child_theme_textdomain( 'critical', get_stylesheet_directory() . '/languages' );

	/* Register additional sidebars. */
	add_action( 'widgets_init', 'critical_register_sidebars' );

	/* Adds the header sidebar to the header. */
	add_action( "{$prefix}_header", 'critical_utility_header' );

	/* Create custom entry meta. */
	add_filter( "{$prefix}_entry_meta", 'critical_entry_meta' );

	/* Add the post ratings plugin to the entry content. */
	add_filter( 'the_content', 'critical_content_rating' );

	/* Disable the entry title. */
	add_filter( "{$prefix}_entry_title", 'critical_disabler' );

	/* Disable the entry byline. */
	add_filter( "{$prefix}_byline", 'critical_disabler' );

	/* Disable the entry excerpt. */
	add_filter( 'the_excerpt', 'critical_disabler' );
}

/**
 * Register additional sidebars.
 *
 * @since 0.1
 */
function critical_register_sidebars() {
	register_sidebar( array( 'name' => __( 'Utility: Header', 'critical' ), 'id' => 'utility-header', 'before_widget' => '<div id="%1$s" class="widget %2$s widget-%2$s"><div class="widget-inside">', 'after_widget' => '</div></div>', 'before_title' => '<h3 class="widget-title">', 'after_title' => '</h3>' ) );
}

/**
 * Insert header widget area.
 * @since 0.1
 */
function critical_utility_header() {
	get_sidebar( 'header' );
}

/**
 * Adds the post ratings to the content on single post views.
 * @link http://wordpress.org/extend/plugins/wp-postratings
 *
 * @since 0.1
 */
function critical_content_rating( $content ) {
	global $post;

	if ( is_singular( 'post' ) && in_the_loop() && function_exists( 'the_ratings' ) )
		$content .= the_ratings( 'div', $post->ID, false );

	return $content;
}

/**
 * Replaces the default entry metadata on archive-type pages with a link to the 
 * comments and post ratings.
 * @link http://wordpress.org/extend/plugins/wp-postratings
 *
 * @since 0.1
 */
function critical_entry_meta( $metadata ) {
	global $post;

	if ( is_singular() || 'link_category' == get_query_var( 'taxonomy' ) )
		return $metadata;

	$metadata = '';

	if ( comments_open() )
		$metadata .= '[entry-comments-link zero="' . __( 'No Responses', 'critical' ) . '"] ';

	if ( function_exists( 'the_ratings' ) )
		$metadata .= the_ratings( 'div', $post->ID, false );

	return $metadata;
}

/**
 * Disabling filter for non-singular pages.
 *
 * @since 0.1
 */
function critical_disabler( $var ) {
	if ( !is_singular() && in_the_loop() )
		return false;

	return $var;
}

?>