<?php
/**
 * Lifestream Sidebar Template
 *
 * Displays the lifestream utility area for the Life Collage theme.
 *
 * @package LifeCollage
 * @subpackage Template
 */
?>
	<?php if ( is_active_sidebar( 'utility-lifestream' ) ) : ?>

		<div id="utility-lifestream" class="sidebar utility">

			<?php dynamic_sidebar( 'utility-lifestream' ); ?>

		</div>

	<?php endif; ?>