<?php
/**
 * Splash Sidebar Template
 *
 * Displays the splash area for the Life Collage theme.
 *
 * @package LifeCollage
 * @subpackage Template
 */
?>
	<div id="splash" class="sidebar utility">

		<?php dynamic_sidebar( 'utility-splash' ); ?>

	</div><!-- #splash .sidebar .utility -->