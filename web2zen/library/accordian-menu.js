jQuery(document).ready(function($) {
    // $() will work as an alias for jQuery() inside of this function
	function initMenu() {
		$('.accordian .children').hide();
		$('.accordian .page_item ul').hide();

		$('.accordian .current-cat .children').show();
		$('.accordian .current-cat-parent .children').show();
		$('.accordian .current-cat-ancestor .children').show();
		$('.accordian .current_page_item ul').show();
		$('.accordian .current_page_ancestor ul').show();
	}
	$(document).ready(function() {initMenu();});
});