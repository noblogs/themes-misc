<?php

/* Header customisation */
define('HEADER_IMAGE_WIDTH', 1000);
define('HEADER_IMAGE_HEIGHT', 159);

define('DEFAULT_TEXTCOLOR', 'fff');
define('HEADER_TEXTCOLOR', DEFAULT_TEXTCOLOR);

define('DEFAULT_IMAGE', get_bloginfo('stylesheet_directory') . '/images/header.jpg');
define('HEADER_IMAGE', DEFAULT_IMAGE); 

function theme_admin_header_style() {
	?>
<style type="text/css">
@import url(<?php bloginfo('stylesheet_directory'); ?>/library/admin-custom-header.css);
</style>
	<?php
}

function theme_header_style() {
if( get_header_image() != DEFAULT_IMAGE || get_header_textcolor() != DEFAULT_TEXTCOLOR ) :
echo '<style type="text/css" media="screen">'."\n";
if( get_header_image() != DEFAULT_IMAGE ) echo '#header {background-image:url(' . get_header_image() . ');}';
if( get_header_textcolor() != DEFAULT_TEXTCOLOR ) echo '#blogtitle,#blogtitle h1,#blogtitle h1 a,#blogtitle h1 small {color:#' . get_header_textcolor(). ';}
';
echo "\n</style>\n\n";
endif;
}
if( function_exists('add_custom_image_header') ) add_custom_image_header('theme_header_style', 'theme_admin_header_style');

?>