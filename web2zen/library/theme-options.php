<?php

// Custom Theme Options
$radios = array('breadcrumb', 'sidebar', 'author_link', 'post_time');
$txts = array();
$ints = array();

$theme_options = array();
if( get_option('theme_options') ) {
	$theme_options = get_option('theme_options');
}
else {
	foreach( $radios as $key ) {
		$theme_options[$key] = 0;
	}
	foreach( $txts as $key ) {
		$theme_options[$key] = '';
	}
	foreach( $ints as $key ) {
		$theme_options[$key] = 0;
	}
}

// Custom theme options CSS
function add_custom_css() {
	$css_file = THEMELIB . '/custom-options.css';
	$css_url = THEMELIB_URI . '/custom-options.css';
	if ( file_exists($css_file) ) {
		wp_register_style('custom-options', $css_url, '', '', 'screen');
	}
}
add_action('admin_init', 'add_custom_css');

function enqueue_css() {
	wp_enqueue_style('custom-options');
}

// Delete theme options
function delete_theme_options($theme_options) {
	delete_option($theme_options);
	echo '<div id="message" class="updated fade"><p>'.__('Your theme options have been deleted.','web2zen').'</p></div>'."\n";
}

// Update theme options
function update_theme_options($theme_options) {
	update_option('theme_options', $theme_options);
	echo '<div id="message" class="updated fade"><p>'.__('Your theme has been updated.','web2zen').'</p></div>'."\n";
}

add_action('admin_menu', 'theme_admin');
if (!function_exists('theme_admin')) {
    // used by the admin panel hook
    function theme_admin() {    
        if (function_exists('add_menu_page')) {
			$page = add_theme_page(__('Theme Options','web2zen'), __('Theme Options','web2zen'),7, basename('theme_options.php'),'theme_admin_style');
			add_action('admin_print_styles-'.$page, 'enqueue_css');
    	}        
    }
}

function theme_admin_style() {
	?>
	<div class="wrap">
	<?php
	echo '<h2>'.__('Theme Options','web2zen').'</h2>'."\n";
	global $wpdb,$radios,$txts,$ints,$theme_options;

	if( isset( $_POST['theme_options_submit'] ) && check_admin_referer($action ='update_options', $query_arg = 'theme_options_form') ) {
		if( $_POST['theme_options_delete'] == 1 ) {
			delete_theme_options('theme_options');
			$theme_options = array();
		}
		else {
			foreach( $radios as $key ) {
				$theme_options[$key] = $_POST[$key];
			}
			foreach( $txts as $key ) {
				$theme_options[$key] = $_POST[$key];
			}
			foreach( $ints as $key ) {
				$theme_options[$key] = absint( intval($_POST[$key]) );
			}
			$theme_options = theme_stripslashes_array($theme_options);
			$theme_options = theme_sanitise_array($theme_options);
			update_theme_options($theme_options);
		}
	}
	?>
	<form method="post" action="" id="theme-options"><div>
	<?php wp_nonce_field( $action = 'update_options', $name = 'theme_options_form', $referer = true , $echo = true) ;?>

	<fieldset>
	<legend><?php _e('Breadcrumb trail','web2zen'); ?></legend>
	<input type="radio" id="theme_breadcrumb_yes" name="breadcrumb" value="0" 
	<?php if(!$theme_options['breadcrumb'] || $theme_options['breadcrumb'] != 1) echo ' checked="checked"';?>
	/> <label for="theme_breadcrumb_yes"><?php _e('Enabled','web2zen'); ?></label> 
	<input type="radio" id="theme_breadcrumb_no" name="breadcrumb" value="1" 
	<?php if ($theme_options['breadcrumb'] == 1) echo ' checked="checked"';?>
	/> <label for="theme_breadcrumb_no"><?php _e('Disabled','web2zen'); ?></label>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display vertical menu','web2zen');?></legend>
	<input type="radio" id="theme_sidebar_yes" name="sidebar" value="0" 
	<?php if ($theme_options['sidebar'] == 0) echo ' checked="checked"';?>
	/> <label for="theme_sidebar_yes"><?php _e('Yes','theme_theme'); ?></label> 
	<input type="radio" id="theme_sidebar_no" name="sidebar" value="1" 
	<?php if ($theme_options['sidebar'] == 1) echo ' checked="checked"';?>
	/> <label for="theme_sidebar_no"><?php _e('No','theme_theme'); ?></label>
	</fieldset>
	
	<fieldset>
	<legend><?php _e('Display author link','web2zen');?></legend>
	<input type="radio" id="theme_author_link_yes" name="author_link" value="0" 
	<?php if ($theme_options['author_link'] == 0) echo ' checked="checked"';?>
	/> <label for="theme_author_link_yes"><?php _e('Yes','theme_theme'); ?></label> 
	<input type="radio" id="theme_author_link_no" name="author_link" value="1" 
	<?php if ($theme_options['author_link'] == 1) echo ' checked="checked"';?>
	/> <label for="theme_author_link_no"><?php _e('No','theme_theme'); ?></label>
	</fieldset>

	<fieldset>
	<legend><?php _e('Display post timestamp','web2zen');?></legend>
	<input type="radio" id="theme_post_time_yes" name="post_time" value="0" 
	<?php if ($theme_options['post_time'] == 0) echo ' checked="checked"';?>
	/> <label for="theme_post_time_yes"><?php _e('Yes','theme_theme'); ?></label> 
	<input type="radio" id="theme_post_time_no" name="post_time" value="1" 
	<?php if ($theme_options['post_time'] == 1) echo ' checked="checked"';?>
	/> <label for="theme_post_time_no"><?php _e('No','theme_theme'); ?></label>
	</fieldset>
	
	<fieldset>
	<legend><?php _e('Reset Theme Options', 'web2zen');?></legend>
	<input type="checkbox" id="theme_options_delete" name="theme_options_delete" value="1" /> 
	<label for="theme_options_delete"><?php _e('Yes','web2zen');?> <strong>(<?php _e('Ticking this box will remove all saved theme options from your database','web2zen');?>)</strong></label>
	</fieldset>
	
	<div>
	<input type="hidden" name="action" value="update" />
	</div>

	<p class="submit"><input type="submit" name="theme_options_submit" class="button-primary" value="<?php _e('Update Theme','web2zen') ?>" /></p>
	</div></form>
</div>
	<?php
}

?>