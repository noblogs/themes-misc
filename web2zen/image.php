<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


<div <?php post_class(); ?>>

<h2><?php the_title(); ?></h2>
<?php edit_post_link(__('Edit','web2zen') ,'<p class="edit">','</p>'); ?>

<div class="attachment">

<a class="thickbox" title="<?php echo get_the_content(); ?> <?php  _e('(press ESC to close)','web2zen');?>" href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'medium' ); ?></a>

<?php the_content(); ?>

<p><?php _e('Posted under','web2zen');?> <a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment"><?php echo get_the_title($post->post_parent); ?></a></p>

<ul class="prevnext">
<li class="next_img"><?php next_image_link();?></li>
<li class="prev_img"><?php previous_image_link(); ?></li>
</ul>
</div>

<?php endwhile; endif; ?>
<!-- end post -->
</div>

<?php get_footer(); ?>