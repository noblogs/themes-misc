<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if(isset($_GET['author_name'])) $curauth = get_userdatabylogin($author_name);
else $curauth = get_userdata(intval($author));?>

<?php if (function_exists('theme_breadcrumb') && $theme_options['breadcrumb'] != 1) echo theme_breadcrumb($curauth->display_name);?>

<div <?php post_class(); ?>>

<h2 class="post-title"><?php echo $curauth->display_name; ?>&rsquo;<?php if( substr($curauth->display_name, -1,1) != 's') echo 's';?> <?php _e('Profile','web2zen');?></h2>

<div class="postcontent">
<?php if( $curauth->user_description !='' && $curauth->user_url !='') :?>
<dl class="author-details">

<?php if( $curauth->user_description !='' ) :?>
<dt class="bio"><?php _e('Bio','web2zen');?>:</dt>
<dd class="bio"><?php echo $curauth->user_description; ?></dd>
<?php endif;?>

<?php if( $curauth->user_url !='' ) :?>
<dt class="website"><?php _e('Website','web2zen');?>:</dt>
<dd class="website"><a href="<?php echo $curauth->user_url;?>"><?php echo $curauth->user_url;?></a></dd>
<?php endif;?>

<?php if(function_exists('get_the_author_meta')) :?>
<?php if( get_the_author_meta('jabber', $curauth->ID) != '') :?>
<dt class="jabber"><?php _e('Jabber / Google Talk','web2zen');?>:</dt>
<dd class="jabber"><?php the_author_meta('jabber', $curauth->ID);?></dd>
<?php endif;?>

<?php if( get_the_author_meta('aim', $curauth->ID) != '') :?>
<dt class="aim"><abbr title="<?php _e('AOL Instant Messenger','web2zen');?>"><?php _e('AIM','web2zen');?></abbr>:</dt>
<dd class="aim"><?php the_author_meta('aim', $curauth->ID);?></dd>
<?php endif;?>

<?php if( get_the_author_meta('yim', $curauth->ID) != '') :?>
<dt class="yim"><?php _e('Yahoo','web2zen');?> <abbr title="<?php _e('Instant Messenger','web2zen');?>"><?php _e('IM','web2zen');?></abbr>:</dt>
<dd class="yim"><?php the_author_meta('yim', $curauth->ID);?></dd>
<?php endif;?>

<?php endif;?>
</dl>
<?php endif;?>
</div>
</div>

<h3 class="post-title"><?php _e('Posts by','web2zen');?> <?php echo $curauth->display_name; ?></h3>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<h4 id="post-<?php the_ID();?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Post <?php the_ID(); ?> - <?php _e('permanent link', 'web2zen');?>"><?php the_title(); ?></a></h4>

<ul class="meta postfoot">
<li><?php _e('Date','web2zen');?>: <?php the_time("F j, Y"); ?></li>
<li><?php _e('Filed under','web2zen');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?>
<li><?php _e('Tags','web2zen');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li>
<?php endif;?>
</ul>

<?php endwhile; ?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts','web2zen') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts','web2zen') );?></li>
</ul>


<?php endif; ?>

<?php get_footer(); ?>