<?php 
// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');
if ( post_password_required() ) {
	echo '<p class="nocomments">'._e('This post is password protected. Enter the password to view comments.','web2zen') .'</p>';
	return;
}
?>

<?php if(have_comments()) : ?>
<h2 id="comments" class="total-comments"><?php comments_number(__('No comments','web2zen') , '1'.__(' Comment','web2zen') , '%'.__(' Comments','web2zen') ); ?> on <?php the_title(); ?></h2>
<?php endif;?>

<?php if ( 'open' == $post->comment_status && have_comments()) : ?>
<ul class="comment-links">
<li><a href="#respond"><?php _e('Add your comment','web2zen');?></a></li>
<?php if ( have_comments() ) : ?><li><a href="<?php echo get_post_comments_feed_link();?>"><?php _e('Comments feed for this post', 'web2zen'); ?></a></li><?php endif;?>
<?php if(pings_open()) : ?><li><a href="<?php trackback_url();?>" rel="trackback"><?php _e('TrackBack <abbr title="Uniform Resource Identifier">URI</abbr>','web2zen');?></a></li><?php endif;?>
</ul>
<?php endif;?>

<?php if(have_comments()) : ?>

<?php paginate_comments_links('type=list'); ?>

<ol id="commentlist">
<?php wp_list_comments(); ?>
</ol>

<?php paginate_comments_links('type=list'); ?>

<?php endif;?>

<?php if('open' == $post->comment_status) : ?>
<div id="respond">
<h3><?php comment_form_title(__( 'Leave a comment','web2zen') , __('Reply to %s','web2zen')  ); ?></h3>
<div class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></div>

<?php if(pings_open()) : ?><p class="trackback"><a href="<?php trackback_url();?>" rel="trackback"><?php _e('TrackBack <abbr title="Uniform Resource Identifier">URI</abbr>','web2zen');?></a></p><?php endif;?>

<p class="comment_log_status">
<?php if (get_option('comment_registration') && !is_user_logged_in() ) : ?><?php _e('You must be','web2zen');?> <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?redirect_to=<?php echo urlencode(get_permalink()); ?>"><?php _e('logged in','web2zen');?></a> <?php _e('to post a comment');?>.
<?php elseif(is_user_logged_in() ) : ?><?php _e('You are currently logged in as','web2zen');?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a> - <a href="<?php echo wp_logout_url(get_permalink()); ?>"><?php _e('Log out','web2zen');?></a><?php endif;?>
</p>

<form action="<?php bloginfo('url'); ?>/wp-comments-post.php" method="post" id="commentform">
<fieldset>

<?php if(!is_user_logged_in()) : ?>
<p><label for="author" <?php if($req) echo 'class="req"';?>><?php _e('Name','web2zen');?> <?php if($req) echo '<small>('.__('required','web2zen') .')</small>';?></label> <input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="20" /></p>
<p><label for="email"<?php if($req) echo 'class="req"';?>><?php _e('Email','web2zen');?>  <?php if($req) echo '<small>('.__('required but will not be published','web2zen') .')</small>'; ?></label> <input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="40" /></p>
<p><label for="url"><?php _e('Website','web2zen');?></label> <input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="40" /></p>
<?php endif; ?>

<p><label for="comment" class="textarea"><span class="req"><?php _e('Comments','web2zen');?> <small>(<?php _e('required','web2zen');?>)</small></span></label>
<textarea name="comment" id="comment" cols="100%" rows="10"></textarea></p>

<p class="submit_wrap"><input name="submit" type="submit" class="submit" value="<?php _e('Submit Comment','web2zen');?>" /> 
<?php do_action('comment_form', $post->ID); ?>
<?php comment_id_fields(); ?></p>
</fieldset>
</form>
</div>

<?php elseif(have_comments()) : ?>
<p class="comments-closed"><strong><?php _e('Sorry, the comment form is now closed.','web2zen');?></strong></p>
<?php endif;?>
