<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head profile="http://gmpg.org/xfn/1">
<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset'); ?>" />

<meta name="description" content="<?php echo theme_meta_description(); ?>" />
<meta name="keywords" content="<?php echo theme_meta_keywords();?>" />

<meta name="designer" content="esmi" />
<meta name="designer_email" content="esmi@quirm.net" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/print.css" media="print" />

<!--[if IE]>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/ie.css" media="screen" type="text/css" />
<![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/ie7.css" media="screen" type="text/css" />
<script src="<?php bloginfo('template_directory'); ?>/library/focus.js" type="text/javascript"></script>
<![endif]-->

<?php
$theme_options = get_option('web2zen_theme_options');
if($theme_options['sidebar'] == 1) :?>
<style type = "text/css">
#content {width:100%;}
</style>
<?php endif;?>

<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_get_archives('type=monthly&format=link'); ?>

<title><?php if(is_search()) {
	if(esc_html(trim($s)) != ' ') {_e(' Search Results for &#39;', 'web2zen'); esc_html_e(trim($s));_e('&#39; on ', 'web2zen');bloginfo('name');}
	else {bloginfo('name');_e(' - No search query entered!', 'web2zen');}
}
elseif (is_category() || is_author()) {wp_title(':',true,'right');bloginfo('name');}
elseif(is_tag()) {_e('Entries tagged with ','web2zen');wp_title('',true);if(wp_title('',false)) {echo ' : ';}	bloginfo('name');}
elseif(is_archive() && function_exists('theme_get_archive_date')) {echo theme_get_archive_date().' : ';bloginfo('name');}
elseif(is_404()) {bloginfo('name');_e(' - Page not found!','web2zen');}
elseif (have_posts()) {wp_title(':',true,'right');bloginfo('name');}
else {bloginfo('name');}?>
</title>

<?php if(is_singular()) wp_enqueue_script( 'comment-reply' );?>
<?php wp_head(); ?>
</head>

<body id="top" <?php if (function_exists('body_class')) body_class(); ?>>

<div id="wrapper">

<ul class="jumplinks">
<li><a href="#content"><?php _e('Jump to Main Content','web2zen');?></a></li>
<li><a href="#right_sidebar"><?php _e('Jump to Side Navigation','web2zen');?></a></li>
<li><a href="#footer"><?php _e('Jump to Footer','web2zen');?></a></li>
</ul>

<div id="header">
<div id="blogtitle"><h1><a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><span><?php bloginfo('name'); ?></span>
<small><?php bloginfo('description'); ?></small></a></h1></div>
</div>

<?php get_sidebar('horizontal'); ?>
