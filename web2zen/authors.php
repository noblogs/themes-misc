<?php
/*
Template Name: List Authors
*/
?>
<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (function_exists('theme_breadcrumb') && $theme_options['breadcrumb'] != 1) echo theme_breadcrumb($post->ID);?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class(); ?>>

<h2 class="post-title" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link', 'web2zen');?>: <?php the_title(); ?>"><?php the_title(); ?></a></h2>
<ul class="meta">
<li><?php edit_post_link(__('Edit','web2zen') ,'<span class="edit">','</span>'); ?></li>
</ul>

<div class="postcontent">
<?php the_content(); ?>

<ul class="authors">
<?php wp_list_authors('exclude_admin=0&optioncount=1'); ?> 
</ul>
</div>

<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages','web2zen').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages','web2zen') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>

</div>

<?php endwhile; ?>

<?php  endif; ?>

<?php get_footer(); ?>