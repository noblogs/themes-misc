Web2Zen Wordpress Theme

INSTALLATION
Simply upload the Web2Zen theme folder to your wp-content themes directory .
Navigate to Admin/Appearance/Themes.
Select the Web2Zen theme.
Select "Activate".


CUSTOMISED META-KEYWORDS
The theme will attempt to automatically generate meta-keyords and meta-descriptions for all posts, pages and archives across your blog using a combination of WordPress titles, tags and descriptions. If you wish to create your own meta-keywords and meta-descriptions, on a post-by-post basis, use the Custom fields area of the Post/Page editing interface to set up 'description' and 'keywords' custom meta-fields.


CHANGING THE HEADER TEXT COLOR
Within the Admin area, navigate to Appearance / Custom Header Image.
Use the "Select A Text Color" to choose a new color for your header's text.
To activate your new color, select "Save Changes".
To reset the color back to the default, select "Use Original Color", then select "Save Changes".
All header text color changes will take place immediately. Remember to press CTRL and F5 simultaneously when viewing the updated site. This should ensure that your web browser fetches a fresh copy of the page rather than serving up an out-dated copy from your own cache.


CHANGING THE HEADER IMAGE
Within the Admin area, navigate to Appearance / Custom Header Image.
Use the Browse button to locate your new header image on your computer.
Once you have selected your new image, click "Upload".
If your new image is larger than the dimensions indicated, you will then be given the opportunity to crop the image to the correct dimensions. Use your cursor to move the highlighted area to the section of the image that you would like to use and click once.
The final image will be saved to your wp-content/uploads folder and will be immediately incorporated into your site's header. 
If you wish to try another image, simply repeat this process.
If you wish to revert to the original image, navigate back to Appearance / Custom Header Image and select "Restore Original Header". Again, all changes will take place immediately.
All header image changes will take place immediately. Remember to press CTRL and F5 simultaneously when viewing the updated site. This should ensure that your web browser fetches a fresh copy of the page rather than serving up an out-dated copy from your own cache.


ADDITIONAL THEME OPTIONS
Within the Admin area, navigate to Appearance / Theme Options and choose from the following options:

- Breadcrumb trail
	The breadcrumb trail displays a list of links (e.g. You are viewing: Home � Uncategorized � Simple gallery test) at the top of each page or post

- Display vertical menu
	Turn the right-hand navigation menu on or off.
	
- Display author link
	Displays the author's name on posts, as a link to their own Author page, at the foot of each post (e.g. Author: Joe).
	
- Display post timestamps
	Display the post publication time (e.g. 11:32 am) as well as the date.
	
- Reset Theme Options
	This will set all theme options back to their default settings and remove your saved options from the database.

All theme option changes will take place immediately. Remember to press CTRL and F5 simultaneously when viewing the updated site. This should ensure that your web browser fetches a fresh copy of the page rather than serving up an out-dated copy from your own cache.


FEATURED POSTS
WordPress 2.7 introduced an option to make posts "sticky".
Sticky Posts are those where the "Stick this post to the front page"  or "Make this post sticky" check box has been checked. In the Web2Zen theme, "sticky" posts are displayed as Featured Posts in a 2 x 2 layout on your blog's front page.
If a custom Excerpt has been created for a "sticky" post, this text will be used on your blog's front page.
If no custom Excerpt exists, an automatic excerpt will be created based upon the first 55 words of the post's content.


FEATURED POST IMAGES
The post thumbnail for each sticky post is automatically displayed.
If no post thumbnail has been set for a given "sticky" post, a default image iwill be used. You can set a post thumbnail for each post in the Edit Post page.


CUSTOM EXCERPT
To add an excerpt to a post, simply write one in the Excerpt field, under the post edit box. It can be as short or as long as you wish. In most cases, given the excerpt�s purpose, a couple of sentences is fine. 


SETTING STICKY POSTS
To flag a post as "sticky", navigate to via Admin/Posts and select the relevant post.
Within the Publish tab, select "Edit" next to "Visibility".
Check the box labeled "Stick this post to the front page".
Select "OK" followed by "Update Post".


WORDPRESS VIDEO TUTORIALS
http://wordpress.tv/


SUPPORT
Available via http://www.forum.quirm.net/