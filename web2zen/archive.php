<?php get_header(); ?>

<div id="content" class="<?php echo content_class();?>">

<?php if (function_exists('theme_breadcrumb') && $theme_options['breadcrumb'] != 1 && function_exists('theme_get_archive_date')) echo theme_breadcrumb(theme_get_archive_date());?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<div <?php if(is_sticky()) post_class('sticky');else post_class();?>id="post-<?php the_ID();?>">
<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Post', 'web2zen');?> <?php the_ID(); ?> -<?php _e(' permanent link', 'web2zen');?>"><?php the_title(); ?></a></h2>

<ul class="meta">
<li><?php the_time('F j, Y'); ?> <?php if($theme_options['post_time'] != 1) :?><?php the_time(); ?><?php endif;?></li>
<li><?php edit_post_link(__('Edit','web2zen') , '', ''); ?></li>
</ul>

<div class="postcontent">
<?php the_content(__('Continue reading ','web2zen')  . the_title('', '', false)); ?>
</div>

<?php 
if(function_exists('theme_link_pages')) theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<div class="pagelist">'.__('Pages','web2zen').':<ul>', 'after' => '</ul></div>', 'next_or_number' => 'number'));
else wp_link_pages('before=<div class="pagelist">'.__('Pages','web2zen') .':&after=</div>&link_before=&link_after=&pagelink=%');
?>

<ul class="meta postfoot">
<?php if('open' == $post->comment_status) : ?><li class="comment_link"><?php comments_popup_link(__('Comment on ','web2zen') .$post->post_title, __('1 Comment on ','web2zen') .$post->post_title, __('% Comments on ','web2zen') .$post->post_title,'postcomment',__('Comments are off for ','web2zen') .$post->post_title); ?></li><?php endif;?>
<?php if($theme_options['author_link'] != 1) :?><li><?php _e('Author','web2zen');?>: <?php the_author_posts_link(); ?></li><?php endif;?>
<li><?php _e('Filed under','web2zen');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?>
<li><?php _e('Tags','web2zen');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li>
<?php endif;?>
</ul>

</div>

<?php endwhile; ?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts','web2zen') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts','web2zen') );?></li>
</ul>

<?php endif; ?>

<?php get_footer(); ?>