<?php get_header();?>

<div id="content" class="<?php echo content_class();?>">

<div <?php post_class('search-results');?>>
<h2 class="post-title"><?php _e('Search Results','web2zen');?></h2>

<?php 
$my_tot_pages = $wp_query->max_num_pages;
if($my_tot_pages ==1) $my_tot_pages.= __(' page', 'web2zen');
else $my_tot_pages .= __(' pages', 'web2zen');
$my_curr_page = $paged;
if($my_curr_page =='') $my_curr_page = 1;
$my_searchterm = trim(esc_html($s));
if($my_searchterm !='') : ?>
<p><?php _e('You searched for', 'web2zen');?> <em><?php echo $my_searchterm;?></em>.</p> 

<?php if (have_posts()) : ?>
<p><?php _e('Displaying page', 'web2zen');?> <?php echo $my_curr_page;?> <?php _e('of', 'web2zen');?> <?php echo $my_tot_pages;?> <?php _e('of results', 'web2zen');?>:</p>

<?php while (have_posts()) : the_post(); ?>

<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link', 'web2zen');?>: <?php the_title(); ?>"><?php the_title(); ?></a></h3>

<ul class="meta postfoot">
<li><?php _e('Date', 'web2zen');?>: <?php the_time('j F Y');?></li>
<?php if($post->post_type == 'post') :?>
<li><?php _e('Categories', 'web2zen');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?><li><?php _e('Tags', 'web2zen');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>
<?php endif;?>
</ul>

<?php endwhile; ?>
</div>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts','web2zen') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts','web2zen') );?></li>
</ul>

<?php else : ?>
<p class="sorry"><?php _e("Sorry - I couldn't find anything on",'web2zen');?> '<em><?php echo $my_searchterm; ?></em>'.</p>
</div>
<?php endif;else : ?>
<p><strong class="error"><?php _e('You forgot to enter a search term','web2zen');?>!</strong></p>
</div>
<?php endif; ?>

<?php get_footer();?>