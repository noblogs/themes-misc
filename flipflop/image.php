<?php get_header(); ?>
<div id="content">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class('image');?>>

<h2 class="post-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
<ul class="meta">
<li><?php the_time(__('F j, Y', 'flipflop') ); ?> <?php the_time(); ?></li>
<li><?php edit_post_link(__('Edit', 'flipflop')); ?></li>
</ul>

<a class="thickbox" title="<?php echo get_the_content(); ?> <?php _e('(press ESC to close)','flipflop');?>" href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'medium' ); ?></a>
<?php the_content(); ?>

<p class="postdate"><?php _e('Posted under', 'flipflop');?> <a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment"><?php echo get_the_title($post->post_parent); ?></a></p>

<ul class="prevnext">
<li class="prev_img"><?php previous_image_link(); ?></li>
<li class="next_img"><?php next_image_link();?></li>
</ul>

</div>
<?php endwhile; endif; ?>

</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>