<?php

// add post_thumbnail support
if ( function_exists( 'add_theme_support' ) ) { 
	add_theme_support( 'post-thumbnails' );
}
define('THUMB_WIDTH', get_option('thumbnail_size_w'));
define('THUMB_HEIGHT', get_option('thumbnail_size_h'));

// Set post thumb dimensions to the values entered in Settings/Media
if ( function_exists( 'set_post_thumbnail_size' ) ) { 
	set_post_thumbnail_size( THUMB_WIDTH, THUMB_HEIGHT ); // box (proportional) resize mode
}

// Set up a proper &hellip;for post extracts
function proper_hellip($more) {
	return '&hellip;';
}
add_filter('excerpt_more', 'proper_hellip');

// Create meta description
function theme_meta_description() {
	$my_output = '';
	$my_excerpt = '';
	global $post;
	switch(true) {
		case (is_category()):
		$my_output .= get_bloginfo('name');
		if(trim(strip_tags(category_description())) !='') echo ': '.trim(strip_tags(category_description()));
		break;
		
		case (is_tag()):
		$my_output .= _e('Entries tagged with ','nh') . single_tag_title('',false)._e(' on ','nh') .get_bloginfo('name');
		break;
		
		case (is_archive()):
		$my_output .= _e('Archives for ','nh') ;
		if(is_year()) $my_output .= get_the_time('Y').' ';
		if(is_month()) $my_output .= get_the_time('F Y').' ';
		if(is_day()) $my_output .= get_the_time('l F Y').' ';
		$my_output .= 'on '.get_bloginfo('name');
		break;
		
		case (is_single() || is_page()):
		if(get_post_meta($post->ID, 'description', true) !='') $my_output .= get_post_meta($post->ID, 'description', true);
		else {
			while(have_posts()):the_post();
			$my_excerpt .= str_replace(array("\r\n", "\r", "\n"), '', get_the_excerpt());
			$my_output .= apply_filters('the_excerpt', $my_excerpt);
			$my_output = strip_tags($my_output);
			endwhile;
		}
		break;
		
		default:
		$my_output .= esc_html( get_bloginfo('name').': '.get_bloginfo('description') );
		
	}
	return $my_output;
}

// Create meta-keywords
function theme_meta_keywords() {
	$my_output = '';
	global $post;
	switch(true) {
		case (is_category()):
		$my_output .= strtolower(single_cat_title());
		break;
		
		case (is_tag()):
		$my_output .= strtolower(single_tag_title());
		break;

		case (is_archive()):
		$my_output .= __('archives ', 'nh') ;
		if(is_year()) $my_output .= get_the_time('Y');
		if(is_month()) $my_output .= strtolower(get_the_time('F Y'));
		if(is_day()) $my_output .= strtolower(get_the_time('l F Y'));
		break;

		case (is_page()):
		$my_output .= strtolower(trim(wp_title('',false,'')));
		if(get_post_meta($post->ID, 'keywords', true) !='') $my_output .= ' '.get_post_meta($post->ID, 'keywords', true);
		break;

		case (is_single()):
		$my_output .= strtolower(trim(wp_title('',false,'')));
		if(get_post_meta($post->ID, 'keywords', true) !='') $my_output .= ' '.get_post_meta($post->ID, 'keywords', true);
		foreach((get_the_category()) as $category) { 
		    $my_output .= ' ' . $category->cat_name; 
		} 

		break;
		
		default:
		$my_output .= esc_html( strtolower( get_bloginfo('name').' '.get_bloginfo('description') ) );
	}
	return $my_output;
}

// Create decent attachment links
function theme_attachment_link($id) {
	// grab file extension
	$bits = explode('.',get_attached_file($id));
	$ext = '.'.$bits[count($bits)-1].' format';
	// get the icon link
	$icon_link = wp_get_attachment_link($id,'thumbnail',false,true);
	// get the text link
	$text_link = wp_get_attachment_link($id,'',false,false);
	// get the filesize in kilobytes
	if(@filesize(get_attached_file($id))) {
		 $filesize = ceil(filesize(get_attached_file($id))/1024).'K';
		 return $icon_link. ' '.$text_link.' <small>('.$ext.' '.$filesize.')</small>';
	}
	else return $icon_link. ' '.$text_link.' <small>('.$ext.')</small>';
}

// Create decent attachment download links
function theme_attachment_download_link($msg, $my_mime_type, $or) {
	// create download link based on attachment mime type
	$download='';
	$viewer_links = 'http://office.microsoft.com/en-gb/downloads/ha010449811033.aspx';
	switch (true) {
		case (stristr($my_mime_type,'pdf')):
		$download = '<a href="http://www.adobe.com/uk/products/reader/">Adobe Acrobat Reader</a>';
		break;

		case (stristr($my_mime_type,'msword')):
		$download = 'Microsoft<sup>&reg;</sup> Word ' . $or . ' <a href="' . $viewer_links . '">Word Viewer</a>';
		break;

		case (stristr($my_mime_type,'excel')):
		$download = 'Microsoft<sup>&reg;</sup> Excel ' . $or . ' <a href="' . $viewer_links . '">Excel Viewer 2003</a>';
		break;

		case (stristr($my_mime_type,'powerpoint')):
		$download = 'Microsoft<sup>&reg;</sup> Powerpoint ' . $or . ' <a href="' . $viewer_links . '">Powerpoint Reader</a>';
		break;

		case (stristr($my_mime_type,'quicktime')):
		$download = '<a href="http://www.apple.com/quicktime/download/">Quicktime</a>';
		break;
	}
	if($download !='') return $msg . $download;
}

/*
theme_link_pages() function taken from ePage Links plugin by Rich Pedley (http://quirm.net/)
Alternative for wp_link_pages to be able to specifiy wrappers for each link. Simply use <code>theme_link_pages(array('blink'=&gt;'&lt;li&gt;','alink'=&gt;'&lt;/li&gt;','before' =&gt; '&lt;ul&gt;', 'after' =&gt; '&lt;/ul&gt;', 'next_or_number' =&gt; 'number'));</code> in place of wp_link_pages in your themes. 
eg. theme_link_pages(array('blink'=>'<li>','alink'=>'</li>','before' => '<ul>', 'after' => '</ul>', 'next_or_number' => 'number')); 
*/
function theme_link_pages($args = '') {
	global $post;

	if (is_array($args) )
		$r = &$args;
	else
		parse_str($args, $r);

	$defaults = array('before' => '<p>' . __('Pages:','nh'), 'after' => '</p>', 'next_or_number' => 'number', 'nextpagelink' => __('Next page','nh'),
			'previouspagelink' => __('Previous page','nh'), 'pagelink' => '%', 'more_file' => '', 'echo' => 1, 'blink'=>'','alink'=>'');
	$r = array_merge($defaults, $r);
	extract($r, EXTR_SKIP);

	global $id, $page, $numpages, $multipage, $more, $pagenow;
	if ($more_file != '' )
		$file = $more_file;
	else
		$file = $pagenow;

	$output = '';
	if ($multipage ) {
		if ('number' == $next_or_number ) {
			$output .= $before;
			for ($i = 1; $i < ($numpages+1); $i = $i + 1 ) {
				$j = str_replace('%',"$i",$pagelink);
				$output .= ' ';
				if (($i != $page) || ((!$more) && ($page==1)) ) {
					if (1 == $i ) {
						$output .= $blink.'<a href="' . get_permalink() . '">'.$j.'</a>'.$alink;
					} else {
						if ('' == get_option('permalink_structure') || 'draft' == $post->post_status )
							$output .= $blink.'<a href="' . get_permalink() . '&amp;page=' . $i . '">'.$j.'</a>'.$alink;
						else
							$output .= $blink.'<a href="' . trailingslashit(get_permalink()) . user_trailingslashit($i, 'single_paged') . '">'.$j.'</a>'.$alink;
					}
				}else{
					$output .= $blink.'<span>'.$j.'</span>'.$alink;
				}
			}
			$output .= $after;
		} else {
			if ($more ) {
				$output .= $before;
				$i = $page - 1;
				if ($i && $more ) {
					if (1 == $i ) {
						$output .= $blink.'<a href="' . get_permalink() . '">' . $previouspagelink . '</a>'.$alink;
					} else {
						if ('' == get_option('permalink_structure') || 'draft' == $post->post_status )
							$output .= $blink.'<a href="' . get_permalink() . '&amp;page=' . $i . '">' . $previouspagelink . '</a>'.$alink;
						else
							$output .= $blink.'<a href="' . trailingslashit(get_permalink()) . user_trailingslashit($i, 'single_paged') . '">' . $previouspagelink . '</a>'.$alink;
					}
				}
				$i = $page + 1;
				if ($i <= $numpages && $more ) {
					if (1 == $i ) {
						$output .= $blink.'<a href="' . get_permalink() . '">' . $nextpagelink . '</a>'.$alink;
					} else {
						if ('' == get_option('permalink_structure') || 'draft' == $post->post_status )
							$output .= $blink.'<a href="' . get_permalink() . '&amp;page=' . $i . '">' . $nextpagelink . '</a>'.$alink;
						else
							$output .= $blink.'<a href="' . trailingslashit(get_permalink()) . user_trailingslashit($i, 'single_paged') . '">' . $nextpagelink . '</a>'.$alink;
					}
				}
				$output .= $after;
			}
		}
	}

	if ($echo )
		echo $output;

	return $output;
}

// remove gallery css
add_filter('gallery_style', function($a) { return "
<div class='gallery'>";});

// Return page tree
function theme_page_tree($this_page) {
	if( !$this_page->post_parent ) $pagelist = wp_list_pages('title_li=&child_of='.$this_page->ID.'&echo=0');
	elseif( $this_page->ancestors ) {
		// get the top ID of this page. Page ids DESC so top level ID is the last one
		$ancestor = end($this_page->ancestors);
		$pagelist = wp_list_pages('title_li=&include='.$ancestor.'&echo=0');
		$pagelist = str_replace('</li>', '', $pagelist);
		$pagelist .= '<ul>' . wp_list_pages('title_li=&child_of='.$ancestor.'&echo=0') .'</ul></li>';
	}
	return $pagelist;
}

// Get archive date
function theme_get_archive_date() {
	if (is_day()) $this_archive = get_the_time('j F Y');
	elseif (is_month()) $this_archive = get_the_time('F Y');
	else $this_archive = get_the_time('Y');
	return $this_archive;
}

// Sanitise an input array
function theme_sanitise_array($array) {
	return is_array($array) ? array_map('theme_sanitise_array', $array) : esc_html($array,ENT_QUOTES);
}

// get category & it's children as an array of ids
function theme_get_cat_tree($parent) {
	$term_objs = get_term_children( $parent, 'category' );
	$term_objs[] =$parent;
	return $term_objs;
}

/* Echoes the wp_list_categories output with extra class applied to the topmost parent category.
*  Relies on theme_top_cats()
* @param string $curr_cat: Current Category ID
* @param string $catlist: Ouput from wp_list_categories
* @param string Optional $extra_class: Additonal class to be added to the li element. Defaults to 'current-cat-ancestor'.
*
**/
function theme_enhanced_cat_list($curr_cat_id,$catlist, $extra_class='current-cat-ancestor') {
        $cat_ancestor_class = "";
	$top_cats = theme_top_cats();
	foreach( $top_cats as $top_cat) {
		if( cat_is_ancestor_of($top_cat, $curr_cat_id) ) {
			$cat_ancestor_class = 'cat-item-'. $top_cat;
			$amended_class = $cat_ancestor_class . ' ' . $extra_class;
			break;
		}
	}
	if( $cat_ancestor_class != "" && stristr( $catlist, $cat_ancestor_class) ) $new_catlist = str_replace($cat_ancestor_class, $amended_class, $catlist );
	else $new_catlist = $catlist;
	echo $new_catlist;
}

// Get ids for all top level (parent) categories
function theme_top_cats() {
	$toplevel = array();
	$allcats = get_terms('category', 'orderby=name&hide_empty=0');
	foreach( $allcats as $cat ) {
		if( $cat->parent == 0) $toplevel[] = $cat->term_id;
	}
	return $toplevel;
}

// add a microid to all the comments
function theme_comment_add_microid($classes) {
	$c_email=get_comment_author_email();
	$c_url=get_comment_author_url();
	if (!empty($c_email) && !empty($c_url)) {
		$microid = 'microid-mailto+http:sha1:' . sha1(sha1('mailto:'.$c_email).sha1($c_url));
		$classes[] = $microid;
	}
	return $classes;
}
add_filter('comment_class','theme_comment_add_microid');

// Display a - b of x posts
function theme_postlist_info($my_tot_pages, $paged, $found_posts) {
	if( $paged == '') $my_curr_page = 1;
	else 	$my_curr_page = $paged;
	$my_finish =  $my_curr_page * get_option('posts_per_page');
	if( $found_posts < $my_finish ) $my_finish = $found_posts;
	$my_start = 1 + $my_finish - get_option('posts_per_page');
	return array($my_start, $my_finish);
}

// is_subpage conditional
function theme_is_subpage() {
	global $post, $wpdb;
	if ( is_page() AND isset( $post->post_parent ) != 0 ) 	{
		$aParent = $wpdb->get_row( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE ID = %d AND post_type = 'page' LIMIT 1", $post->post_parent ) );
		if ( $aParent->ID ) return true; else return false;
	}
	else return false;
}

?>
