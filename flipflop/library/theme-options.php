<?php

// FlipFlop Custom Theme Options
$radios = array('custom_logo', 'logo_display', 'color', 'hide_kses', 'accordian');
$txts = array('logo_url');
$ints = array('custom_excerpt');

$theme_options = array();
if( get_option('flipflop_options') ) {
	$theme_options = get_option('flipflop_options');
}
else {
	foreach( $radios as $key ) {
		$theme_options[$key] = 0;
		$theme_options['color'] = 'default';
	}
	foreach( $txts as $key ) {
		$theme_options[$key] = '';
	}
	foreach( $ints as $key ) {
		$theme_options[$key] = 0;
	}
}

// Custom theme options CSS
function add_custom_css() {
	$css_file = THEMELIB . '/custom-options.css';
	$css_url = THEMELIB_URI . '/custom-options.css';
	if ( file_exists($css_file) ) {
		wp_register_style('custom-options', $css_url, '', '', 'screen');
	}
}
add_action('admin_init', 'add_custom_css');

function enqueue_css() {
	wp_enqueue_style('custom-options');
}

// Delete theme options
function delete_theme_options($theme_options) {
	delete_option($theme_options);
	echo '<div id="message" class="updated fade"><p>'.__('Your theme options have been deleted.','flipflop').'</p></div>'."\n";
}

// Update theme options
function update_theme_options($theme_options) {
	update_option('flipflop_options', $theme_options);
	echo '<div id="message" class="updated fade"><p>'.__('Your theme has been updated.','flipflop').'</p></div>'."\n";
}

add_action('admin_menu', 'theme_admin');
if (!function_exists('theme_admin')) {
    // used by the admin panel hook
    function theme_admin() {    
        if (function_exists('add_menu_page')) {
			$page = add_theme_page(__('Theme Options','flipflop'), __('Theme Options','flipflop'),7, basename('theme_options.php'),'theme_admin_style');
			add_action('admin_print_styles-'.$page, 'enqueue_css');
    	}        
    }
}

function theme_admin_style() {
	?>
	<div class="wrap">
	<?php
	echo '<h2>'.__('Theme Options','flipflop').'</h2>'."\n";
	global $wpdb,$radios,$txts,$ints,$theme_options;

	if( isset( $_POST['theme_options_submit'] ) && check_admin_referer($action ='update_options', $query_arg = 'theme_options_form') ) {
		if( $_POST['theme_options_delete'] == 1 ) {
			delete_theme_options('flipflop_options');
			$theme_options = array();
		}
		else {
			if( get_magic_quotes_gpc() ) $_POST = array_map( 'stripslashes_deep', $_POST );
			foreach( $radios as $key ) {
				$theme_options[$key] = $_POST[$key];
			}
			foreach( $txts as $key ) {
				$theme_options[$key] = $_POST[$key];
			}
			foreach( $ints as $key ) {
				$theme_options[$key] = absint( intval($_POST[$key]) );
			}
			$theme_options = theme_sanitise_array($theme_options);
			update_theme_options($theme_options);
		}
	}
	?>
	<form method="post" action="" id="theme-options"><div>
	<?php wp_nonce_field( $action = 'update_options', $name = 'theme_options_form', $referer = true , $echo = true) ;?>

	<fieldset>
	<legend><?php _e('Theme Color','flipflop');?></legend>
	<input type="radio" id="color_default" name="color" value="default" <?php if($theme_options['color'] == 'default') echo ' checked="checked"';?> /> <label for="color_default"><?php _e('Default','flipflop');?></label>  
	<input type="radio" id="color_blue" name="color" value="blue" <?php if($theme_options['color'] == 'blue') echo ' checked="checked"';?> /> <label for="color_blue"><?php _e('Blue','flipflop');?></label>  
	<input type="radio" id="color_green" name="color" value="green" <?php if($theme_options['color'] == 'green') echo ' checked="checked"';?> /> <label for="color_green"><?php _e('Green','flipflop');?></label>  
	<input type="radio" id="color_grey" name="color" value="grey" <?php if($theme_options['color'] == 'grey') echo ' checked="checked"';?> /> <label for="color_grey"><?php _e('Grey','flipflop');?></label>  
	<input type="radio" id="color_gold" name="color" value="gold" <?php if($theme_options['color'] == 'gold') echo ' checked="checked"';?> /> <label for="color_gold"><?php _e('Gold','flipflop');?></label>  
	<input type="radio" id="color_red" name="color" value="red" <?php if($theme_options['color'] == 'red') echo ' checked="checked"';?> /> <label for="color_red"><?php _e('Red','flipflop');?></label>  
	</fieldset>

	<fieldset>
	<legend><?php _e('Display Logo','flipflop'); ?></legend>
	<input type="radio" id="logo_display_yes" name="logo_display" value="0" <?php if(!$theme_options['logo_display'] || $theme_options['logo_display'] != 1) echo ' checked="checked"';?> /> 
	<label for="logo_display_yes"><?php _e('Yes','flipflop'); ?></label> 
	<input type="radio" id="logo_display_no" name="logo_display" value="1" <?php if ($theme_options['logo_display'] == 1) echo ' checked="checked"';?> /> <label for="logo_display_no"><?php _e('No','flipflop'); ?></label>
	</fieldset>

	<fieldset>
	<legend><?php _e('Logo Type','flipflop'); ?></legend>
	<input type="radio" id="logo_default" name="custom_logo" value="0" 	<?php if(!$theme_options['custom_logo'] || $theme_options['custom_logo'] != 1) echo ' checked="checked"';?> /> 
	<label for="logo_default"><?php _e('Default Image','flipflop'); ?></label> 
	<input type="radio" id="logo_custom" name="custom_logo" value="1" <?php if ($theme_options['custom_logo'] == 1) echo ' checked="checked"';?> /> <label for="logo_custom"><?php _e('Custom Image','flipflop'); ?></label>
	
	<p><label for="logo_url" class="logo-url"><?php _e('Custom Logo URL','flipflop'); ?></label>
	<input type="text" id="logo_url" name="logo_url" value="<?php if ($theme_options['logo_url'] && $theme_options['logo_url'] !='') echo $theme_options['logo_url'];?>" /><br />
	<label for="logo_url" class="logo-size"><?php printf(__('Custom logo images should be %1$d x %2$d pixels'), LOGO_IMAGE_WIDTH,LOGO_IMAGE_HEIGHT); ?></label></p>
	</fieldset>

	<fieldset>
	<legend><?php _e('Post Excerpt Length','flipflop');?></legend>
	<label for="custom_excerpt"><?php _e('Length (in characters):','flipflop');?></label> 
	<input class="excerpt" type="text" id="custom_excerpt" name="custom_excerpt" maxlength="3" value="<?php if( $theme_options['custom_excerpt'] =='' ) echo '100' ;else echo $theme_options['custom_excerpt'];?>" />
	</fieldset>
	
	<fieldset>
	<legend><?php _e('Allowed Comment tags','flipflop');?></legend>
	<input type="radio" id="hide_kses_no" name="hide_kses" value="0" <?php if(!$theme_options['hide_kses'] || $theme_options['hide_kses'] == 0) echo ' checked="checked"';?> /> <label for="hide_kses_no"><?php _e('Display','flipflop');?></label>  
	<input type="radio" id="hide_kses_yes" name="hide_kses" value="1" <?php if($theme_options['hide_kses'] != 0) echo ' checked="checked"';?>  /> <label for="hide_kses_yes"><?php _e('Hide','flipflop');?></label>
	</fieldset>
	
	<fieldset>
	<legend><?php _e('Collapsing sub-menus','flipflop'); ?></legend>
	<input type="radio" id="theme_accordian_yes" name="accordian" value="0" 
	<?php if(!$theme_options['accordian'] || $theme_options['accordian'] != 1) echo ' checked="checked"';?>
	/> <label for="theme_accordian_yes"><?php _e('Enable','flipflop'); ?></label> 
		<input type="radio" id="theme_accordian_no" name="accordian" value="1" 
	<?php if ($theme_options['accordian'] == 1) echo ' checked="checked"';?>
	/> <label for="theme_accordian_no"><?php _e('Disable','flipflop'); ?></label>
	</fieldset>

	<fieldset>
	<legend><?php _e('Reset Theme Options', 'flipflop');?></legend>
	<input type="checkbox" id="theme_options_delete" name="theme_options_delete" value="1" /> 
	<label for="theme_options_delete"><?php _e('Yes','flipflop');?> <strong>(<?php _e('Ticking this box will remove all saved theme options from your database','flipflop');?>)</strong></label>
	</fieldset>
	
	<div>
	<input type="hidden" name="action" value="update" />
	</div>

	<p class="submit"><input type="submit" name="theme_options_submit" class="button-primary" value="<?php _e('Update Theme','flipflop') ?>" /></p>
	</div></form>
</div>
	<?php
}

?>