<?php

/* Header customisation */
define('HEADER_IMAGE_WIDTH', 543);
define('HEADER_IMAGE_HEIGHT', 170);
define('LOGO_IMAGE_WIDTH', 120);
define('LOGO_IMAGE_HEIGHT', 120);

define('DEFAULT_TEXTCOLOR', 'FDBC0A');
define('HEADER_TEXTCOLOR', DEFAULT_TEXTCOLOR);

define('DEFAULT_IMAGE', get_bloginfo('stylesheet_directory') . '/images/header.jpg');
define('HEADER_IMAGE', DEFAULT_IMAGE); 

if( get_option('flipflop_options') ) $theme_options = get_option('flipflop_options');
else $theme_options = array();

function theme_admin_header_style() {
	?>
<style type="text/css">
@import url(<?php bloginfo('template_directory'); ?>/library/admin-custom-header.css);
</style>
	<?php
}

function get_theme_options() {
	global $theme_options;
	$logo_display = $logo_image = '';
	switch ($theme_options['color']) {
		case 'green':
			@define('HEADER_TEXTCOLOR', '978F46');
			@define('HEADER_IMAGE', '%s/images/green-header.jpg');
			break;

		case 'gold':
			@define('HEADER_TEXTCOLOR', 'FEC10E');
			@define('HEADER_IMAGE', '%s/images/gold-header.jpg');
			break;

		default:
			@define('HEADER_TEXTCOLOR', 'FDBC0A');
			@define('HEADER_IMAGE', '%s/images/header.jpg');
	}
	if($theme_options['logo_display'] && $theme_options['logo_display'] == 1) $logo_display = "#header h1 {left:-10px;}\n#logo {display:none;}\n";
	if($theme_options['custom_logo'] == 1 && $theme_options['logo_url'] !='') $logo_image = '#logo {background-image:url('.$theme_options['logo_url'].');}'."\n";
	return array($logo_display,$logo_image);
}

function theme_header_style() {
	list($logo_display,$logo_image) = get_theme_options();
	if( get_header_image() != DEFAULT_IMAGE || get_header_textcolor() != DEFAULT_TEXTCOLOR ) :
	echo '<style type="text/css" media="screen">'."\n";
	if( get_header_image() != DEFAULT_IMAGE ) echo '#header {background-image:url(' . get_header_image() . ');}';
	if( get_header_textcolor() != DEFAULT_TEXTCOLOR ) echo '#header h1 {color:#' . get_header_textcolor() . ';}';
	echo "\n</style>\n";
	endif;
	if( $logo_display != '' || $logo_image != '' ) :
	echo '<style type="text/css" media="screen">'."\n";
	if( $logo_display != '') echo $logo_display;
	if( $logo_image != '' ) echo $logo_image;
	echo "\n</style>\n";
	endif;
}
if( function_exists('add_custom_image_header') ) add_custom_image_header('theme_header_style', 'theme_admin_header_style');

?>
