<?php

// Add thickbox to public-facing image attachment pages only
function thickbox_init() {
	if (is_attachment() && substr(get_post_mime_type(), 0,5) == 'image') {
		add_thickbox();
	}
}
add_action('template_redirect', 'thickbox_init');

// Correct image path issue in thickbox
function load_tb_fix() {
	echo "\n" . '<script type="text/javascript">tb_pathToImage = "' . get_option('siteurl') . '/wp-includes/js/thickbox/loadingAnimation.gif";tb_closeImage = "' . get_option('siteurl') . '/wp-includes/js/thickbox/tb-close.png";</script>'. "\n";
}
add_action('wp_footer', 'load_tb_fix');

function add_dark_style() {
	$css_file = THEMELIB . '/dark-thickbox.css';
	$css_url = THEMELIB_URI . '/dark-thickbox.css';
	if ( file_exists($css_file) ) {
		wp_register_style('dark-thickbox', $css_url, '', '', 'screen');
		wp_enqueue_style('dark-thickbox');
	}
}
add_action('wp_print_styles', 'add_dark_style');

?>