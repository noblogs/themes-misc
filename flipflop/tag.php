<?php get_header(); ?>

<div id="content">

<?php wp_tag_cloud('format=list&unit=em&largest=2&smallest=1&number=0'); ?>

<?php
$this_feed = get_bloginfo('url'). '?tag=' . single_tag_title('',false) . '&amp;feed=rss2
';
?>

<h1 class="tag-title"><a title="<?php _e('RSS 2 feed for ', 'flipflop');single_tag_title();?>" href="<?php echo $this_feed;?>"><?php _e('Entries tagged with');?> '<?php single_tag_title(); ?>'</a></h1>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div <?php post_class();?> id="post-<?php the_ID();?>">
<h2 class="post-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link', 'flipflop');?>: <?php the_title(); ?>"><?php the_title(); ?></a></h2>

<ul class="meta">
<li><?php the_time(__('F j, Y', 'flipflop') ); ?> <?php the_time(); ?></li>
<li><?php edit_post_link(__('Edit', 'flipflop')); ?></li>
</ul>

<?php if( has_post_thumbnail() ) {
	the_post_thumbnail();
	the_excerpt();
}
else the_content(''); ?>

<ul class="meta postfoot">
<li><a href="<?php the_permalink();?>"><?php _e('Continue reading', 'flipflop');?> <?php the_title();?></a></li>
<?php if('open' == $post->comment_status) : ?><li><?php comments_popup_link(__('Comment on ', 'flipflop') .$post->post_title, __('1 Comment on ', 'flipflop') .$post->post_title, __('% Comments on ', 'flipflop') .$post->post_title,'postcomment',__('Comments are off for ', 'flipflop') .$post->post_title); ?></li><?php endif;?>
<li><?php _e('Filed under:', 'flipflop');?> <ul><li><?php the_category(',</li> <li>'); ?></li></ul></li>
</ul>

</div>

<?php endwhile; ?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts', 'flipflop') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts', 'flipflop') );?></li>
</ul>

<?php endif; ?>

</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>