<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta name="generator" content="WordPress" />
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<meta name="description" content="<?php echo theme_meta_description(); ?>" />
<meta name="keywords" content="<?php echo theme_meta_keywords();?>" />
								  
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />

<?php
$theme_options = get_option('flipflop_options');
if( !file_exists( get_template_directory() . '/' . $theme_options['color'] . '.css') ) $color = 'color';
else $color = $theme_options['color'];
?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/<?php echo $color;?>.css" media="screen" />

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/print.css" media="print" />

<!--[if IE]><link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/ie.css" media="screen" type="text/css" /><![endif]-->
<!--[if lte IE 7]>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/ie7.css" media="screen" type="text/css" />
<script src="<?php bloginfo('template_directory'); ?>/library/focus.js" type="text/javascript"></script>
<![endif]-->

<?php if(is_singular()) :?><script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/addPrintPage.js"></script><?php endif;?>

<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name');_e(': RSS 2.0', 'flipflop');?>" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="text/xml" title="<?php bloginfo('name');_e(': RSS .92', 'flipflop');?>" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name');_e(': Atom 0.3', 'flipflop');?>" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<?php wp_get_archives('type=monthly&format=link'); ?>

<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />

<title><?php if(is_home()) bloginfo('name'); 
elseif (is_search()) {
	if(esc_html(trim($s)) != '') {_e(' Search Results for &#39;', 'flipflop'); esc_html_e(trim($s));_e('&#39; on ', 'flipflop');bloginfo('name');}
	else {bloginfo('name');_e(' - No search query entered!', 'flipflop');}
}
elseif (is_404()) {bloginfo('name'); _e(': Page not found!', 'flipflop') ;}
elseif (is_tag()) {_e(' Entries listed under &#39;', 'flipflop') ; single_tag_title(); _e('&#39; on ', 'flipflop');bloginfo('name');}
elseif (have_posts()) {wp_title('',true);if(wp_title('',false)) {echo ':';}	bloginfo('name');}
else {bloginfo('name');}?>
</title>

<?php if(is_singular()) wp_enqueue_script( 'comment-reply' );?>
<?php wp_head(); ?>
</head>

<body id="top" <?php if (function_exists('body_class')) body_class(); ?>>

<div id="wrapper">

<ul class="jumplinks">
<li><a href="#content"><?php _e('Jump to page content', 'flipflop');?></a></li>
<li><a href="#sidebar"><?php _e('Jump to side navigation', 'flipflop');?></a></li>
<li><a href="#footer"><?php _e('Jump to footer', 'flipflop');?></a></li>
</ul>

<div id="header" onclick="location.href='<?php bloginfo('url'); ?>';"
style="cursor: pointer;"><span id="logo"></span>
<h1><?php bloginfo('name'); ?><br /><small><?php bloginfo('description'); ?></small></h1>

</div>

<?php get_sidebar('top'); ?>
<?php get_sidebar('top2'); ?>
