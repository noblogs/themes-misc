<?php
/*
Template Name: Archives
*/
?>

<?php get_header(); ?>

<div id="content">

<div <?php post_class();?>>
<h2 class="post-title"><?php _e('Articles by Month', 'flipflop');?>:</h2>
<ul>
<?php wp_get_archives('type=monthly'); ?>
</ul>

<h2 class="post-title"><?php _e('Articles by Subject', 'flipflop');?>:</h2>
<ul>
<?php wp_list_categories(); ?>
</ul>
</div>

</div>	
<?php get_sidebar(); ?>

<?php get_footer(); ?>