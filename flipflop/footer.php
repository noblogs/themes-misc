<ul id="icons">
<li class="rss"><a href="#"><img src="<?php bloginfo('template_directory'); ?>/images/rss.jpg" width="32" height="26" title="<?php _e('Subscribe to the site feed', 'flipflop');?>" alt="<?php _e('RSS', 'flipflop');?>" /></a></li>
</ul>

<?php $theme_data = get_theme_data(get_stylesheet_directory() . '/style.css');?>

<div id="footer">
<ul>
<li><?php _e('Powered by <a href="http://www.autistici.org">R*</a> | ', 'flipflop');?> <a href="<?php echo $theme_data['URI'];?>"><?php echo get_current_theme();?> <?php _e('Theme', 'flipflop');?></a></li>
<?php wp_register('<li id="admin">', '</li>'); ?>
</ul>

<?php wp_footer(); ?>
</div>

<!-- end wrapper -->
</div>
</body>
</html>
