<?php get_header(); ?>
<div id="content">

<div class="hentry">
<h2 class="post-title"><?php _e('Search Results');?></h2>

<?php 
$my_tot_pages = $wp_query->max_num_pages;
if($my_tot_pages ==1) $my_tot_pages.= __(' page', 'flipflop');
else $my_tot_pages .= __(' pages', 'flipflop');
$my_curr_page = $paged;
if($my_curr_page =='') $my_curr_page = 1;
$my_searchterm = trim(esc_html($s));
if($my_searchterm !='') : ?>
<p><?php _e('You searched for', 'flipflop');?> <span class="searchterm">'<?php echo $my_searchterm;?>'</span>.</p> 

<?php if (have_posts()) : ?>
<p><?php _e('Displaying page', 'flipflop');?> <?php echo $my_curr_page;?> <?php _e('of', 'flipflop');?> <?php echo $my_tot_pages;?> <?php _e('of results', 'flipflop');?>:</p>

<?php while (have_posts()) : the_post(); ?>	
<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'flipflop');?> <?php the_title(); ?>"><?php the_title(); ?></a></h3>

<?php if($post->post_type == 'post') :?>
<ul class="meta">
<li><?php _e('Filed under', 'flipflop');?>: <ul><li><?php the_category(',</li> <li>') ?></li></ul></li>
<?php if(get_the_tag_list()) :?><li><?php _e('Tags', 'flipflop');?>: <?php the_tags('<ul><li>',',</li> <li>','</li></ul>');?></li><?php endif;?>
</ul>
<?php endif;?>

<?php endwhile; ?>

<ul class="prevnext">
<li class="next"><?php next_posts_link(__('Older Posts', 'flipflop') ); ?></li>
<li class="prev"><?php previous_posts_link(__('Newer Posts', 'flipflop') );?></li>
</ul>

<?php else : ?>
<p class="sorry"><?php _e("Sorry - I couldn't find anything!", 'flipflop');?></p>
<?php endif;else : ?>
<p><strong class="error"><?php _e('You forgot to enter a search term', 'flipflop');?>!</strong></p>
<?php endif; ?>

<br class="clear-left" />

<!-- end post -->
</div>

<!-- end content -->
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>