<?php ob_start(); ?>
<?php header("HTTP/1.1 404 Not Found"); ?>
<?php header("Status: 404 Not Found"); ?>

<?php $requested = 'http';
if(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on') $requested .= 's';
$requested .= '://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$bits = explode('/',$requested);
$poss_slug = $bits[count($bits)-2];
$poss_posts = array();
if(get_posts('name='.$poss_slug)) $poss_posts = get_posts('name='.$poss_slug);
?>

<?php get_header(); ?>
<div id="content">

<div <?php post_class('page');?>>
<h2 class="post-title"><?php _e('Error!', 'flipflop');?></h2>
<p><?php _e("I can't find", 'flipflop');?>:</p><p><strong><?php echo $requested;?></strong></p>

<?php if(count($poss_posts) > 0) :?>
<p><?php _e('But I did find', 'flipflop');?>:</p><ul>
<?php foreach($poss_posts as $post ) : ?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach; ?>
</ul>

<?php else:?>
<p><?php _e('Perhaps you', 'flipflop');?>:</p>
<ul>
<li><?php _e('tried to access a page or entry which has been removed?', 'flipflop');?></li>
<li><?php _e('followed a bad link?', 'flipflop');?></li>
<li><?php _e('mis-typed something?', 'flipflop');?></li>
</ul>
<p><?php _e('Try locating the page you need using the navigation menus or the site Search.', 'flipflop');?></p>
<?php endif;?>

<p class="prev"><a href="<?php bloginfo('url');?>"><?php _e('Home', 'flipflop');?></a></p>
</div>

</div>
<?php get_sidebar(); ?>

<?php get_footer(); ?>
